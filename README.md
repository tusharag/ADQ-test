# YetAnotherCrypto code challenge

## Description

At YetAnotherCrypto Inc we have created the ultimate cryptocurrency to rule them all. 
In order to make the crypto adoption more appealing we have decided to design a system where prices 
go up or down based on demand.

After some analysis, we launch with the following specifications: 
- If no cryptos were purchased in the last 2 hours, the prices should fall by 10%. This discount is just once, not cumulative
- If there are more than 5 purchases in the last hour, the prices should increase by 10%. This increase is just once, not cumulative
- Otherwise, the price is the original one.

## What to do

Given the crypto:
  - Crypto with code `DOGGY` and price of `1500` AED.

Implement a small service that will be consumed for our internal systems and:
- Gets the price of the crypto, with an entrypoint:
  `HTTP GET /cryptos/{crypto-code}/price`
  Responses: `HTTP 200` with body `{ "current-price": 1500 }` or `HTTP 404`         
  This operation should be time complexity o(1)

- Expose an endpoint that will be used to notify us when a purchase has been made:
 `HTTP PUT /cryptos/{crypto-code}/purchases` and body `{ "purchase-ref": "{purchase-ref}", "purchase-date":"2018-09-16T08:00:00" }` 
  Response `HTTP 202`

## Considerations
- We are going to look for good design and good architecture
- Use a dumb InMemory db, don’t use any real db or library.
- You can include any library you want, except for persistence. 

Have fun!
