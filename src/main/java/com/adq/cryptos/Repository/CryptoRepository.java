package com.adq.cryptos.Repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adq.cryptos.entity.CryptoEntity;

@Repository
public interface CryptoRepository extends JpaRepository<CryptoEntity, Long> {
	
	List<CryptoEntity> findByNameAndPurchasedAtBetween(String name,LocalDateTime start,LocalDateTime end);

}
