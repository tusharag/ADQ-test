package com.adq.cryptos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adq.cryptos.controller.dto.ClientPurchase;
import com.adq.cryptos.controller.dto.CryptoPrice;
import com.adq.cryptos.service.CryptoService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class CryptoBookingController {
	
	@Autowired
	private CryptoService cryptoService;

    @GetMapping("/cryptos/{cryptoCode}/price")
    public CryptoPrice getPrice(@PathVariable String cryptoCode) {
        return cryptoService.getCryptoPrice(cryptoCode);

    }

    @PutMapping(value = "/cryptos/{cryptoCode}/purchases")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void registerPurchase(
            @PathVariable String cryptoCode,
            @RequestBody ClientPurchase purchase
    ) {
        cryptoService.registerPurchase(cryptoCode, purchase);
    }
}
