package com.adq.cryptos.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

import java.io.IOException;
import java.time.Instant;

@Data
@Builder
public class ClientPurchase {

    @JsonProperty("purchase-ref")
    private String purchaseRef;

    @JsonProperty("purchase-date")
    @JsonDeserialize(using = InstantDeserializer.class)
    private Instant purchaseDate;

    static class InstantDeserializer extends JsonDeserializer<Instant> {

        @Override
        public Instant deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return Instant.parse(jsonParser.getText());
        }
    }
}
