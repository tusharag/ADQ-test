package com.adq.cryptos.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class CryptoPrice {

    @JsonProperty("current-price")
    private BigDecimal currentPrice;
}
