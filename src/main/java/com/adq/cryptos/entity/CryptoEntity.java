package com.adq.cryptos.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adq.cryptos.entity.base.AuditableEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="crypto")
@Getter
@Setter
public class CryptoEntity extends AuditableEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private LocalDateTime purchasedAt;
	
	private Double price;
	
	private String purchaseRef;
	

}
