package com.adq.cryptos.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adq.cryptos.entity.base.AuditableEntity;

@Entity
@Table(name="purchase_history")
public class CryptoPurchaseHistory extends AuditableEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

}
