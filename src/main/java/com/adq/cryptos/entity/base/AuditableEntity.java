package com.adq.cryptos.entity.base;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class AuditableEntity {
	
	@CreatedDate
	protected LocalDateTime createdAt;

	@LastModifiedDate
	protected LocalDateTime modifiedAt;

	@PrePersist
	public void prePersist() {
		this.createdAt = this.modifiedAt = LocalDateTime.now();
	}

	@PreUpdate
	public void preUpdate() {
		this.modifiedAt = LocalDateTime.now();
	}
}
