package com.adq.cryptos.service;

import com.adq.cryptos.controller.dto.ClientPurchase;
import com.adq.cryptos.controller.dto.CryptoPrice;

public interface CryptoService {

	public CryptoPrice getCryptoPrice(String name);
	
	public void registerPurchase(String name, ClientPurchase purchase);
}
