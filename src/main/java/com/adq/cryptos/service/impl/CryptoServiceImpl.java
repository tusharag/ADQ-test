package com.adq.cryptos.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.adq.cryptos.Repository.CryptoRepository;
import com.adq.cryptos.constants.CryptoConstants;
import com.adq.cryptos.controller.dto.ClientPurchase;
import com.adq.cryptos.controller.dto.CryptoPrice;
import com.adq.cryptos.entity.CryptoEntity;
import com.adq.cryptos.service.CryptoService;

@Service
public class CryptoServiceImpl implements CryptoService {

	@Autowired
	private CryptoRepository cryptoRepository;

	@Override
	public CryptoPrice getCryptoPrice(String name) {

		if (CryptoConstants.DOGGY.equalsIgnoreCase(name)) {

			List<CryptoEntity> cryptoPurchasedLast2Hrs = cryptoRepository.findByNameAndPurchasedAtBetween(name,
					Instant.now().minus(2, ChronoUnit.HOURS).atZone(ZoneOffset.UTC).toLocalDateTime(),
					Instant.now().atZone(ZoneOffset.UTC).toLocalDateTime());

			if (cryptoPurchasedLast2Hrs.isEmpty()) {
				return CryptoPrice.builder()
						.currentPrice(
								BigDecimal.valueOf(CryptoConstants.DOGGY_VALUE - (0.1 * CryptoConstants.DOGGY_VALUE))
										.setScale(0, RoundingMode.DOWN))
						.build();
			}

			else if (cryptoPurchasedLast2Hrs.size() > 5) {

				return CryptoPrice.builder()
						.currentPrice(
								BigDecimal.valueOf(CryptoConstants.DOGGY_VALUE + (0.1 * CryptoConstants.DOGGY_VALUE))
										.setScale(0, RoundingMode.DOWN))
						.build();
			}

			else {
				return CryptoPrice.builder()
						.currentPrice(BigDecimal.valueOf(CryptoConstants.DOGGY_VALUE).setScale(0, RoundingMode.DOWN))
						.build();
			}
		} else
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

	}

	@Override
	@Transactional
	public void registerPurchase(String name, ClientPurchase purchase) {
		if (CryptoConstants.DOGGY.equalsIgnoreCase(name)) {
			CryptoEntity crypto = new CryptoEntity();
			crypto.setName(name);
			crypto.setPrice(CryptoConstants.DOGGY_VALUE);
			crypto.setPurchaseRef(purchase.getPurchaseRef());
			crypto.setPurchasedAt(purchase.getPurchaseDate().atZone(ZoneOffset.UTC).toLocalDateTime());
			cryptoRepository.save(crypto);
		} else
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

}
